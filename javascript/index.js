var TemplateBot = require('./src/template_bot');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var config = {
  host: serverHost,
  port: serverPort,
  name: botName,
  key: botKey,
  multiple: true
};

bot = new TemplateBot(config);
bot.connect();