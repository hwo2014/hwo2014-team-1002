var net = require("net");
var JSONStream = require('JSONStream');

var TemplateBot = function(config) {
  this._port = config.port;
  this._host = config.host;
  this._key = config.key;
  this._name = config.name;
  this._multiple = true;

  this._client = null;
  this._stream = null;

  this._raceColor = null;

  this._connected = false;

  console.log("I'm", this._name, "and connect to", this._host + ":" + this._port);
};

TemplateBot.prototype = {
  connect: function(callback) {
    this._client = net.connect(this._port, this._host, function() {
      this.onConnect();

      if (typeof callback == 'function') {
        callback();
      }
    }.bind(this));
  },

  onConnect: function() {
    this._connected = true;

    // bind
    this._stream = this._client.pipe(JSONStream.parse());

    this._stream.on('data', this.messageHandler.bind(this));
    this._stream.on('error', this.errorHandler.bind(this));

    if (this._multiple) {
      this.send({
        msgType: "join",
        data: {
          name: this._name,
          key: this._key
        }
      });
    } else {
      this.send({
        msgType: "createRace",
        data: {
          botId: {
            name: this._name,
            key: this._key
          },
          trackName: 'keimola',
          password: 'randomstuff',
          carCount: 3
        }
      });
    }
  },

  getOwnPosition: function(data) {
    for (var i=0; i<data.data.length; i++) {
      if (data.data[i].id.color == this._raceColor) {
        return data.data[i];
      }
    }
  },

  messageHandler: function(data) {
    if (data.msgType === 'carPositions') {
      // implement throttle logic here
      var throttle = 0.6;
      var pos = this.getOwnPosition(data);

      console.log(pos.angle);
      if (pos.angle > 10 && pos.angle < -10) {
        throttle = 0.5
      }

      this.send({
        msgType: "throttle",
        data: throttle
      });
    } else {
      switch (data.msgType) {

        case 'join':
          console.log('Joined');
          break;

        case 'yourCar':
          this._raceColor = data.data.color;
          console.log('We are '+data.data.name+' sitting in '+data.data.color);
          break;

        case 'gameStart':
          console.log('Race started');
          break;

        case 'gameEnd':
          console.log('Race ended');
          break;

        case 'gameInit':
          console.log('Game Init');
          console.log(data.data.race.track.pieces)
          break;

        case 'tournamentEnd':
          console.log('Tournament ended');
          break;

        case 'crash':
          console.log(data.data.color+' left the track');
          break;

        case 'spawn':
          console.log(data.data.color+' respawned on track');
          break;

        case 'lapFinished':
          console.log(data.data.car.color+' finished a lap');
          break;

        case 'dnf':
          console.log(data.data.color+' did not finish');
          break;

        case 'finish':
          console.log(data.data.color+' finished the race');
          break;
      }

      this.send({
        msgType: "ping",
        data: {}
      });
    }
  },

  errorHandler: function(data) {
    console.log('Disconnected, trying to reconnect');
    this._connected = false;

    // try reconnecting
    this.connect();
  },

  send: function(json) {
    this._client.write(JSON.stringify(json) + '\n');
  }
};

module.exports = TemplateBot;